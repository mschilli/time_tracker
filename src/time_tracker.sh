#!/bin/sh

# time_tracker core script
# Copyright (C) 2020, 2021  Marcel Schilling
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#######################
# general information #
#######################

# file:        time_tracker.sh
# created:     2020-05-04
# last update: 2021-11-04
# author:      Marcel Schilling <marcel.schilling@mdc-berlin.de>
# license:     GNU Affero General Public License Version 3 (GNU AGPL v3)
# purpose:     Core script for time_tracker tool.


######################################
# change log (reverse chronological) #
######################################

# 2021-11-04:
# * New feature:
#   * Track mobile work times.
# * Refactoring:
#   * Reduce dependencies: Replace `cut` by `awk` calls.
#   * Remove unused variable.
# * Documentation:
#   * Fix typo in comment.
# * Style:
#   * Remove superfluous newline.
# 2021-11-03:
# * New feature:
#   * Track commute & work times.
# 2020-05-15:
# * New feature:
#   * Query current status.
# * Documentation:
#   * Add missing `.` to comment.
# 2020-05-11:
# * Fix:
#   * Convert event time stamp to internal format for TSV validation.
# 2020-05-08:
# * New feature:
#   * Validate input TSV.
#     * Probably not complete.
#     * Only consider latest entries (event day).
# 2020-05-07:
# * Redesign:
#   * Save time stamps as ISO-8601 format.
#     * Preserves time zone information.
#     * Keep seconds resolution.
# * Refactoring:
#   * Introduce error message prefix.
# * Style:
#   * Remove superfluous newline escape.
#     * Awk's `&&` breaks lines without `\`.
# 2020-05-06:
# * Redesign:
#   * Save time stamps as UNIX time instead of ISO-8601 format.
#     * Simplifies downstream use (arithmetics).
# * Fixes:
#   * Remove bash-ism (`[ -v VAR]`) for POSIX shell compatibility.
#   * Move HTTP request responses to STDERR.
#   * Check TSV file existence.
#   * Check TSV file creation.
#   * Ensure TSV file is (or points to) a regular file.
#   * Ensure TSV file is writeable.
# * Refactoring:
#   * Refer to all TSV columns by quoted variables.
#   * Introduce activity status.
#     * Abstracts Slack presence.
#   * Introduce post-event interval type.
#     * Abstracts Slack status text prefix.
#   * Introduce post-event time stamp.
#     * Abstracts Slack status time stamp.
#   * Use abstractions instead of Slack specific variables.
#   * Rename activity status for consistency with other post-event variables.
#   * Move post-event variables to beginning of Slack section.
# * Documentation:
#   * Re-structure change log:
#     * Summarize per day.
#     * Group by type.
#     * Order by impact (groups) and time (commits, chronological).
# 2020-05-05:
# * New features:
#   * Update Slack status text via OAuth token (optional).
#   * Update Slack status emoji.
#   * Update Slack presence.
#   * Track (phone/video) calls.
# 2020-05-04:
# * Initial version:
#   * Start and stop home office time tracking.
# * New feature:
#   * Track breaks.


#########
# usage #
#########

# Command line arguments.
ARGS="<command> <TSV-file> [Slack-token]"

# Usage string.
USAGE='Usage:
  '"$0 $ARGS"


########
# help #
########

# Help string.
HELP="$USAGE"'

Commands:
  start: Start working in remotely or mobile.
  stop: Stop working in remotely or mobile.
  pause: Start a break.
  continue: Continue working after a break.
  status: Query current status.
  call: Start a (phone/video) call.
  hangup: Continue working after a call.
  commute_to_work: Start commuting to work (inactive).
  commute_home: Start commuting from work (inactive).
  commute: Start commuting from/to work (inactive).
  arrive: Start working or end the work day after a commute.

Parameters:
  TSV-file: File to read/write time tracking data from/to.
  Slack-token (optional): OAuth token to update Slack status with.
                          Requires `users.profile:write` and `users.write` user
                          token scopes.

Note: Only some sanity checks are made at this point. Issuing commands in the
wrong order could invalidate the TSV file.
'

##############
# parameters #
##############

# Command line arguments passed to `date` to format time stamps for the time
# tracking TSV file.
TSV_TIMESTAMP_FMT_ARGS='--iso-8601=seconds'

# String used to format time stamps internally (for arithmetics) using `date`.
INTERNAL_TIMESTAMP_FMT='%s'

# Command used to start time tracking.
START_COMMAND='start'

# Command used to stop time tracking.
STOP_COMMAND='stop'

# Command used to start a break.
PAUSE_COMMAND='pause'

# Command used to continue working after a break.
CONTINUE_COMMAND='continue'

# Command used to query status.
STATUS_COMMAND='status'

# Command used to start a (phone/video) call.
CALL_COMMAND='call'

# Command used to continue working after a call.
HANGUP_COMMAND='hangup'

# Command used to start commuting to work.
COMMUTE_TO_WORK_COMMAND='commute_to_work'

# Command used to start commuting from work.
COMMUTE_HOME_COMMAND='commute_home'

# Command used to start commuting from/to work.
COMMUTE_COMMAND='commute'

# Command used to end a commute from/to work.
ARRIVE_COMMAND='arrive'

# String used to indicate the begin of a time interval.
BEGIN_INDICATOR="begin"

# String used to indicate the end of a time interval.
END_INDICATOR="end"

# String used to indicate a home office interval.
INTERVAL_TYPE_HOMEOFFICE="home office"

# String used to indicate after-hours.
INTERVAL_TYPE_END_OF_DAY="after-hours"

# String used to indicate a break interval.
INTERVAL_TYPE_BREAK="break"

# String used to indicate a call interval.
INTERVAL_TYPE_CALL="call"

# String used to indicate a commute interval.
INTERVAL_TYPE_COMMUTE="commute"

# String used to indicate a work interval.
INTERVAL_TYPE_WORK="work"

# String used to indicate a mobile work interval.
INTERVAL_TYPE_MOBILE_WORK="mobile work"

# String used to indicate an active interval.
ACTIVITY_STATUS_ACTIVE="active"

# String used to indicate an inactive interval.
ACTIVITY_STATUS_INACTIVE="inactive"

# Activity status used during home office intervals.
ACTIVITY_STATUS_HOMEOFFICE="$ACTIVITY_STATUS_ACTIVE"

# Activity status used during break intervals.
ACTIVITY_STATUS_BREAK="$ACTIVITY_STATUS_INACTIVE"

# Activity status used during call intervals.
ACTIVITY_STATUS_CALL="$ACTIVITY_STATUS_ACTIVE"

# Activity status used during commute intervals.
ACTIVITY_STATUS_COMMUTE="$ACTIVITY_STATUS_INACTIVE"

# Activity status used during work intervals.
ACTIVITY_STATUS_WORK="$ACTIVITY_STATUS_ACTIVE"

# Activity status used during mobile work intervals.
ACTIVITY_STATUS_MOBILE_WORK="$ACTIVITY_STATUS_ACTIVE"

# Activity status used after stopping time tracking.
ACTIVITY_STATUS_END_OF_DAY="$ACTIVITY_STATUS_INACTIVE"

# String used to format time for status message using `date`.
STATUS_TIME_FMT='%H:%M'

# String used to separate interval time from (in-)activity time in status
# message (if necessary).
STATUS_TIME_SEPARATOR='; '

# String used as prefix for status message during a home office interval.
STATUS_MESSAGE_PREFIX_HOMEOFFICE="home office"

# String used as prefix for Slack status text during a home office interval.
STATUS_TEXT_PREFIX_HOMEOFFICE="$STATUS_MESSAGE_PREFIX_HOMEOFFICE"

# Standard emoji code of emoji used for Slack status during a home office
# interval.
# See http://emoji-cheat-sheet.com.
STATUS_EMOJI_HOMEOFFICE=":house:"

# String used as prefix for status message during a break interval.
STATUS_MESSAGE_PREFIX_BREAK="on a break"

# String used as prefix for Slack status text during a break interval.
STATUS_TEXT_PREFIX_BREAK="$STATUS_MESSAGE_PREFIX_BREAK"

# Standard emoji code of emoji used for Slack status during a break interval.
# See http://emoji-cheat-sheet.com.
STATUS_EMOJI_BREAK=":pouting_cat:"

# String used as prefix for status message during a call interval.
STATUS_MESSAGE_PREFIX_CALL="on a call"

# String used as prefix for Slack status text during a call interval.
STATUS_TEXT_PREFIX_CALL="$STATUS_MESSAGE_PREFIX_CALL"

# Standard emoji code of emoji used for Slack status during a call interval.
# See http://emoji-cheat-sheet.com.
STATUS_EMOJI_CALL=":phone:"

# String used as prefix for status message during a commute interval.
STATUS_MESSAGE_PREFIX_COMMUTE="commuting"

# String used as suffix for prefix for status message during a commute
# to work interval.
STATUS_MESSAGE_PREFIX_SUFFIX_COMMUTE_TO_WORK=" to work"

# String used as prefix for status message during a commute to work
# interval.
STATUS_MESSAGE_PREFIX_COMMUTE_TO_WORK="$STATUS_MESSAGE_PREFIX_COMMUTE"\
"$STATUS_MESSAGE_PREFIX_SUFFIX_COMMUTE_TO_WORK"

# String used as prefix for Slack status text during a commute to work
# interval.
STATUS_TEXT_PREFIX_COMMUTE_TO_WORK="$STATUS_MESSAGE_PREFIX_COMMUTE_TO_WORK"

# String used as suffix for prefix for status message during a commute
# from work interval.
STATUS_MESSAGE_PREFIX_SUFFIX_COMMUTE_HOME=" home"

# String used as prefix for status message during a commute from work
# interval.
STATUS_MESSAGE_PREFIX_COMMUTE_HOME="$STATUS_MESSAGE_PREFIX_COMMUTE"\
"$STATUS_MESSAGE_PREFIX_SUFFIX_COMMUTE_HOME"

# String used as prefix for Slack status text during a commute from work
# interval.
STATUS_TEXT_PREFIX_COMMUTE_HOME="$STATUS_MESSAGE_PREFIX_COMMUTE_HOME"

# Standard emoji code of emoji used for Slack status during a commute
# interval.
# See http://emoji-cheat-sheet.com.
STATUS_EMOJI_COMMUTE=":steam_locomotive:"

# String used as prefix for status message during a work interval.
STATUS_MESSAGE_PREFIX_WORK="working"

# String used as prefix for Slack status text during a work interval.
STATUS_TEXT_PREFIX_WORK="$STATUS_MESSAGE_PREFIX_WORK"

# Standard emoji code of emoji used for Slack status during a work
# interval.
# See http://emoji-cheat-sheet.com.
STATUS_EMOJI_WORK=":keyboard:"

# String used as prefix for status message during a mobile work
# interval.
STATUS_MESSAGE_PREFIX_MOBILE_WORK="working mobile"

# String used as prefix for Slack status text during a mobile work
# interval.
STATUS_TEXT_PREFIX_MOBILE_WORK="$STATUS_MESSAGE_PREFIX_MOBILE_WORK"

# Standard emoji code of emoji used for Slack status during a mobile
# work interval.
# See http://emoji-cheat-sheet.com.
STATUS_EMOJI_WORK=":computer:"

# String used as prefix for status message after stopping time tracking.
STATUS_MESSAGE_PREFIX_END_OF_DAY="not working"

# String used as prefix for Slack status text after stopping time tracking.
STATUS_TEXT_PREFIX_END_OF_DAY="$STATUS_MESSAGE_PREFIX_END_OF_DAY"

# Standard emoji code of emoji used for Slack status after stopping time
# tracking.
# See http://emoji-cheat-sheet.com.
STATUS_EMOJI_END_OF_DAY=":scream_cat:"

# Column number in the time tracking TSV to read/write time stamps from/to.
TSV_COLUMN_TIMESTAMP=1

# Column number in the time tracking TSV to read/write interval types from/to.
TSV_COLUMN_INDICATOR=2

# Column number in the time tracking TSV to read/write interval types from/to.
TSV_COLUMN_INTERVAL_TYPE=3

# String used to format the suffix for Slack status text using `date`.
STATUS_TEXT_TIMESTAMP_FMT="(since %H:%M %Z)"

# Method used for HTTP request.
REQUEST_METHOD='POST'

# Method used for authorization.
AUTH_METHOD='Bearer'

# Name of HTTP header used for authorization.
AUTH_HEADER_NAME='Authorization'

# Name of HTTP header used for request content type specification.
CONTENT_HEADER_NAME='Content-type'

# (Internet) media (cf. MIME) type used for request content.
CONTENT_TYPE_TYPE='application'

# (Internet) media (cf. MIME) subtype used for request content.
CONTENT_TYPE_SUBTYPE='json'

# Name of (internet) media (cf. MIME) type parameter used to specify character
# set.
CHARSET_PARAM_NAME='charset'

# Character set used for request content.
CHARSET='utf-8'

# Name of JSON object used to specify user profile.
USER_PROFILE_OBJECT_NAME='profile'

# Name of JSON object used to specify status text.
STATUS_TEXT_OBJECT_NAME='status_text'

# Name of JSON object used to specify status emoji.
STATUS_EMOJI_OBJECT_NAME='status_emoji'

# Name of JSON object used to specify status expiration.
STATUS_EXPIRATION_OBJECT_NAME='status_expiration'

# Unix time (i.e. seconds since the epoch) to set status expiration to.
# Zero means no expiration.
STATUS_EXPIRATION=0

# URI prefix of API host to send Slack HTTP requests to
HOST_URI_PREFIX='https://slack.com/api/'

# URI suffix of API host to send Slack user profile HTTP requests to.
HOST_URI_SUFFIX_STATUS='users.profile.set'

# Name of JSON object used to specify user presence.
USER_PRESENCE_OBJECT_NAME='presence'

# Slack presence setting used during active intervals.
PRESENCE_SETTING_ACTIVE='auto'

# Slack presence setting used during inactive intervals.
PRESENCE_SETTING_INACTIVE='away'

# URI suffix of API host to send Slack user presence HTTP requests to.
HOST_URI_SUFFIX_PRESENCE='users.setPresence'


############
# messages #
############

# String use prefix error messages with.
ERROR_MESSAGE_PREFIX="ERROR:"

# Message to use for invalid command error.
INVALID_COMMAND_MESSAGE='invalid command:'

# Message to use for missing TSV file error.
MISSING_TSV_MESSAGE='Time tracking TSV file does not exists:'

# Message to use for TSV file creation error.
FAILURE_TO_CREATE_TSV_MESSAGE='Time tracking TSV cannot be created:'

# Message to use for TSV file not being (or pointing to) a regular file error.
TSV_NOT_A_FILE_MESSAGE='Time tracking TSV file not a regular file (nor a '\
'symlink pointing to one):'

# Message to use for TSV file not being readable error.
TSV_NOT_READABLE_MESSAGE='Time tracking TSV file not readable :'

# Message to use for TSV file not being writable error.
TSV_NOT_WRITEABLE_MESSAGE='Time tracking TSV file not writeable :'

# Message to use for invalid time stamp error.
INVALID_TIMESTAMP_MESSAGE="Invalid timestamp: '"

# Message to use for future event error.
FUTURE_EVENT_MESSAGE="Future event: '"

# Message to use for out of order event error.
EVENT_OUT_OF_ORDER_MESSAGE="Event out of order: '"

# Message to use for ivalid interval type error.
INVALID_INTERVAL_TYPE_MESSAGE="Invalid interval type: '"

# Message to use for ivalid indicator error.
INVALID_INDICATOR_MESSAGE="Invalid interval boundary indicator: '"

# Message to use for nested top interval error.
NESTED_TOP_LEVEL_INTERVAL_MESSAGE='Interval contains nested top level '\
"interval: '"

# Message to use for conflicting interval type error.
CONFLICTING_INTERVAL_TYPE_MESSAGE='Interval type of begin does not match that '\
"of corresponding end: '"

# Message to use for conflicting activity status error.
CONFLICTING_ACTIVITY_STATUS_MESSAGE="Inactive interval contains active one: '"

# Message to use for loose interval end error.
LOOSE_END_MESSAGE="Interval closed but never opened: '"

# Message to use for invalid TSV file error.
INVALID_TSV_MESSAGE='Invalid TSV file (see above)!'

# Message to use for unreachable statement error.
UNREACHABLE_STATEMENT_MESSAGE='Unreachable statement reached!'

# Message to use for unclear commute direction error.
UNCLEAR_COMMUTE_DIRECTION_MESSAGE='Unclear in which direction to commute.\n'\
'Use `commute_to_work` or `commute_home` commands to explicitly state the '\
'direction.'

# Message to use for invalid pre-commute interval error.
INVALID_PRE_COMMUTE_INTERVAL_MESSAGE="Invalid pre-commute interval: '"

# Message to use for invalid pre-arrival interval error.
INVALID_PRE_ARRIVAL_INTERVAL_MESSAGE="Invalid pre-arrival interval: '"

# Message to use for invalid pre-home office interval error.
INVALID_PRE_HOMEOFFICE_INTERVAL_MESSAGE="Invalid pre-home office interval: '"

# Message to use for unclear interval to stop error.
UNCLEAR_INTERVAL_TO_STOP_MESSAGE='Unclear which interval to stop.
Current interval: '


#############
# functions #
#############

# Print message to STDOUT and exit with zero code.
print_help() {
  printf '%s\n' "$@"
  exit 0
}

# Print error message to STDERR and exit with non-zero code.
print_error() {
  printf '%s %s\n' "$ERROR_MESSAGE_PREFIX" "$@" >&2
  exit 1
}


########
# help #
########

# Abort with help message if called with help flag.
if [ $# -eq 1 ] && { [ "$1" = "--help" ] || [ "$1" = "-h" ]; }
then
  print_help "$HELP"
fi


###########################
# command line validation #
###########################

# Count minimal expected number of command line arguments (excluding optional
# parameters).
EXPECTED_ARGC_MIN=$(printf '%s' "$ARGS" | sed 's/\[[^[]\+\]//' | wc --words)

# Count maximal expected number of command line arguments (including optional
# parameters).
EXPECTED_ARGC_MAX=$(printf '%s' "$ARGS" | wc --words)

# Abort with usage error unless an expected number of command line arguments
# was given.
[ $# -ge "$EXPECTED_ARGC_MIN" ] && [ $# -le "$EXPECTED_ARGC_MAX" ] || \
  print_error "$USAGE"


##########################
# command line arguments #
##########################

# Command to run.
COMMAND="$1"

# TSV file to read/write time tracking data from/to.
TIME_TRACKING_TSV="$2"

# OAuth token to update Slack status with.
# Requires `users.profile:write` user token scope.
if [ $# -ge 3 ]
then
  SLACK_TOKEN="$3"
fi


####################################
# command line argument validation #
####################################

# Abort with invalid command error unless a valid command was given.
[ "$COMMAND" = "$START_COMMAND" ] || \
  [ "$COMMAND" = "$STOP_COMMAND" ] || \
  [ "$COMMAND" = "$PAUSE_COMMAND" ] || \
  [ "$COMMAND" = "$CONTINUE_COMMAND" ] || \
  [ "$COMMAND" = "$STATUS_COMMAND" ] || \
  [ "$COMMAND" = "$CALL_COMMAND" ] || \
  [ "$COMMAND" = "$HANGUP_COMMAND" ] || \
  [ "$COMMAND" = "$COMMUTE_TO_WORK_COMMAND" ] || \
  [ "$COMMAND" = "$COMMUTE_HOME_COMMAND" ] || \
  [ "$COMMAND" = "$COMMUTE_COMMAND" ] || \
  [ "$COMMAND" = "$ARRIVE_COMMAND" ] || \
  print_error "$INVALID_COMMAND_MESSAGE '$COMMAND'"


######################
# TSV file existence #
######################

# Abort with missing TSV error unless file exists or `start` command was given.
[ -e "$TIME_TRACKING_TSV" ] || [ "$COMMAND" = "$START_COMMAND" ] || \
  print_error "$MISSING_TSV_MESSAGE '$TIME_TRACKING_TSV'"


#####################
# TSV file creation #
#####################

# Create (empty) time tracking TSV file if it does not exist already and abort
# with error upon failure.
[ -e "$TIME_TRACKING_TSV" ] || touch "$TIME_TRACKING_TSV" || \
  print_error "$FAILURE_TO_CREATE_TSV_MESSAGE '$TIME_TRACKING_TSV'"


#################
# TSV file type #
#################

# Ensure time tracking TSV file is (or points to) a regular file.
[ -f $(realpath "$TIME_TRACKING_TSV") ] || \
  print_error "$TSV_NOT_A_FILE_MESSAGE '$TIME_TRACKING_TSV'"


########################
# TSV file permissions #
########################

# Ensure time tracking TSV file is readable.
[ -r "$TIME_TRACKING_TSV" ] || \
  print_error "$TSV_NOT_READABLE_MESSAGE '$TIME_TRACKING_TSV'"

# Ensure time tracking TSV file is writeable unless `status` command was given.
[ -w "$TIME_TRACKING_TSV" ] || [ "$COMMAND" = "$STATUS_COMMAND" ] \
  print_error "$TSV_NOT_WRITEABLE_MESSAGE '$TIME_TRACKING_TSV'"


###############
# time stamps #
###############

# Get event time stamp.
EVENT_TIMESTAMP=$(date "$TSV_TIMESTAMP_FMT_ARGS")

# Truncate event time stamp to date (ISO-8601 format) to get the beginning of
# the event day.
DAY_START_TIMESTAMP=$(date --date="$EVENT_TIMESTAMP" --iso-8601=date)

# Convert the beginning of the event day to internal time stamp format.
DAY_START_TIMESTAMP=$(date --date="$DAY_START_TIMESTAMP" '+'"$INTERNAL_TIMESTAMP_FMT")


#############
# event day #
#############

# Check all TSV entries since the beginning of the event day (in reverse
# order).
# Include corresponding begins for cross-midnight intervals.
# Return pre-event data.
# Abort with error if the TSV is found invalid.
PRE_EVENT_DATA=$(
  tac "$TIME_TRACKING_TSV" \
  | awk \
      --field-separator='\t' \
      --assign=OFS='\t' \
      --assign=event_timestamp="$EVENT_TIMESTAMP" \
      --assign=tsv_column_timestamp="$TSV_COLUMN_TIMESTAMP" \
      --assign=tsv_column_indicator="$TSV_COLUMN_INDICATOR" \
      --assign=tsv_column_interval_type="$TSV_COLUMN_INTERVAL_TYPE" \
      --assign=interval_type_homeoffice="$INTERVAL_TYPE_HOMEOFFICE" \
      --assign=interval_type_end_of_day="$INTERVAL_TYPE_END_OF_DAY" \
      --assign=interval_type_break="$INTERVAL_TYPE_BREAK" \
      --assign=interval_type_call="$INTERVAL_TYPE_CALL" \
      --assign=interval_type_commute="$INTERVAL_TYPE_COMMUTE" \
      --assign=interval_type_work="$INTERVAL_TYPE_WORK" \
      --assign=interval_type_mobile_work="$INTERVAL_TYPE_MOBILE_WORK" \
      --assign=activity_status_active="$ACTIVITY_STATUS_ACTIVE" \
      --assign=activity_status_homeoffice="$ACTIVITY_STATUS_HOMEOFFICE" \
      --assign=activity_status_break="$ACTIVITY_STATUS_BREAK" \
      --assign=activity_status_call="$ACTIVITY_STATUS_CALL" \
      --assign=activity_status_commute="$ACTIVITY_STATUS_COMMUTE" \
      --assign=activity_status_work="$ACTIVITY_STATUS_WORK" \
      --assign=activity_status_end_of_day="$ACTIVITY_STATUS_END_OF_DAY" \
      --assign=begin_indicator="$BEGIN_INDICATOR" \
      --assign=end_indicator="$END_INDICATOR" \
      --assign=day_start_timestamp="$DAY_START_TIMESTAMP" \
      --assign=internal_timestamp_fmt="$INTERNAL_TIMESTAMP_FMT" \
      --assign=error_message_prefix="$ERROR_MESSAGE_PREFIX" \
      --assign=invalid_timestamp_message="$INVALID_TIMESTAMP_MESSAGE" \
      --assign=future_event_message="$FUTURE_EVENT_MESSAGE" \
      --assign=event_out_of_order_message="$EVENT_OUT_OF_ORDER_MESSAGE" \
      --assign=invalid_interval_type_message="$INVALID_INTERVAL_TYPE_MESSAGE" \
      --assign=invalid_indicator_message="$INVALID_INDICATOR_MESSAGE" \
      --assign=nested_top_level_interval_message="$NESTED_TOP_LEVEL_INTERVAL_MESSAGE" \
      --assign=conflicting_interval_type_message="$CONFLICTING_INTERVAL_TYPE_MESSAGE" \
      --assign=conflicting_activity_status_message="$CONFLICTING_ACTIVITY_STATUS_MESSAGE" \
      --assign=loose_end_message="$LOOSE_END_MESSAGE" \
      '
        #############
        # functions #
        #############

        # Print error message to STDERR and exit with non-zero code.
        # Note: The (global) previous error check guards from secondary errors
        # triggered in the END rule (which is still run after an error exit to
        # allow for proper cleanup).
        function print_error(message) {
          if(!previous_error) {
            printf "%s %s\n", error_message_prefix, message > "/dev/stderr"
            previous_error = 1
          }
          exit 1
        }

        # Convert time stamp to internal format (for arithmetics).
        # Abort with error if conversion fails.
        function convert_timestamp(timestamp_to_convert) {
          timestamp_conversion_call = \
            "date --date='"'"'" timestamp_to_convert "'"'"' '"'"'+" \
            internal_timestamp_fmt "'"'"'"
          if(!(timestamp_conversion_call | getline converted_timestamp)) {
            print_error(invalid_timestamp_message timestamp_to_convert "'"'"'")
          }
          close(timestamp_conversion_call)
          return(converted_timestamp)
        }


        ##################
        # initialization #
        ##################

        BEGIN{

          # Convert event time stamp to internal format (for arithmetics).
          event_timestamp = convert_timestamp(event_timestamp)

          # Define valid interval types.
          interval_types[interval_type_homeoffice]
          interval_types[interval_type_break]
          interval_types[interval_type_call]
          interval_types[interval_type_commute]
          interval_types[interval_type_work]
          interval_types[interval_type_mobile_work]

          # Define valid top level interval types.
          interval_type_is_top_levels[interval_type_homeoffice]
          interval_type_is_top_levels[interval_type_commute]
          interval_type_is_top_levels[interval_type_work]

          # Map interval types to their corresponding activity status.
          interval_type2activity_status[interval_type_homeoffice] = \
            activity_status_homeoffice
          interval_type2activity_status[interval_type_break] = \
            activity_status_break
          interval_type2activity_status[interval_type_call] = \
            activity_status_call
          interval_type2activity_status[interval_type_commute] = \
            activity_status_commute
          interval_type2activity_status[interval_type_work] = \
            activity_status_work
          interval_type2activity_status[interval_type_mobile_work] = \
            activity_status_work

          # Initialize pre-event data with pre-start values (i.e. after-hours).
          pre_event_interval_begin_timestamp = day_start_timestamp
          pre_event_interval_type = interval_type_end_of_day
          pre_event_interval_is_active = activity_status_end_of_day == \
             activity_status_active

          # Process the last entry as if the current event was processed
          # already.
          previous_timestamp = event_timestamp
        }


        ###############
        # TSV parsing #
        ###############

        {

          # Split TSV entry into fields.
          timestamp = $tsv_column_timestamp
          indicator = $tsv_column_indicator
          interval_type = $tsv_column_interval_type


        ####################
        # time stamp check #
        ####################

          # Convert time stamp to internal format (for arithmetics).
          timestamp = convert_timestamp(timestamp)
        }


        ######################
        # event day complete #
        ######################

        # Exit once the previous day has been reached and all interval begins
        # have been parsed.
        timestamp < day_start_timestamp && top_level_reached {
          exit
        }


        ####################
        # sort order check #
        ####################

        # Abort with error if time stamp is in the future or out of order.
        timestamp > previous_timestamp {
          if(NR == 1) {
            print_error(future_event_message $0 "'"'"'")
          }
          print_error(event_out_of_order_message $0 "'"'"'")
        }


        #######################
        # interval type check #
        #######################

        !(interval_type in interval_types) {
          print_error(invalid_interval_type_message $0 "'"'"'")
        }


        ###############
        # valid entry #
        ###############

        {
          # Update (previous) entry features.
          previous_timestamp = timestamp
          interval_is_active = \
            interval_type2activity_status[interval_type] == \
              activity_status_active
          interval_type_is_top_level = \
            interval_type in interval_type_is_top_levels


        #################
        # nesting check #
        #################

          # Check if interval is contained in another one.
          parent_depth = depth - 1
          has_parent = parent_depth in depth2interval_type
        }


        #########################
        # activity status check #
        #########################

        # Abort with error if active interval is contained within an inactive
        # one.
        has_parent && interval_is_active {
          parent_interval_type = depth2interval_type[parent_depth]
          parent_interval_is_active = \
            interval_type2activity_status[parent_interval_type] == \
              activity_status_active
          if(!parent_interval_is_active && \
             !(parent_interval_type == interval_type_commute && \
               interval_type == interval_type_mobile_work)) {
            print_error(conflicting_activity_status_message $0 "'"'"'")
          }
        }


        ######################
        # new interval (end) #
        ######################

        # Add new interval to ongoing intervals stack for each processed
        # interval end.
        indicator == end_indicator {
          depth2end_entry[++depth] = $0
          depth2interval_type[depth] = interval_type
          depth2end_timestamp[depth] = timestamp


        ##########################
        # top-level interval end #
        ##########################

          # Enable multiple time tracking periods on the same day.
          if(interval_type_is_top_level) {
            top_level_reached = 0


        ####################
        # end of event day #
        ####################

          # Keep pre-event interval as after-hours type but overwrite its begin
          # when processing an event-day top-level type interval end.
            if(pre_event_interval_begin_timestamp == day_start_timestamp &&
                 timestamp >= day_start_timestamp) {
              pre_event_interval_begin_timestamp = timestamp
            }
          }


        ###################
        # indicator check #
        ###################

          # All necessary information for interval ends has been processed at
          # this point.
          next
        }

        # Abort with error if indicator is neither a valid end indicator
        # (handled above), nor a valid begin indicator (handled below).
        indicator != begin_indicator{
          print_error(invalid_indicator_message $0 "'"'"'")
        }


        #############################
        # nested top-level interval #
        #############################

        # Abort with error if interval contains top-level type interval.
        top_level_reached {
          print_error(nested_top_level_interval_message $0 "'"'"'")
        }


        ############################
        # top-level interval begin #
        ############################

        # Do not allow intervals to contain top level intervals.
        interval_type_is_top_level {
          top_level_reached = 1
        }


        ########################
        # interval match check #
        ########################

        {
          # Abort with error if interval begin and end types do not match.
          if(depth in depth2interval_type) {
            if(interval_type != depth2interval_type[depth]) {
              print_error(conflicting_interval_type_message $0 "'"'"'")
            }


        ######################
        # completed interval #
        ######################

            # Set interval end time stamp based on corresponding TSV entry.
            end_timestamp = depth2end_timestamp[depth]

            # Remove completed interval from ongoing intervals stack.
            delete depth2end_entry[depth]
            delete depth2interval_type[depth]
            delete depth2end_timestamp[depth]


        ####################
        # ongoing interval #
        ####################

          } else { # !(depth in depth2interval_type)

            # Set interval end time stamp based on event time stamp.
            end_timestamp = event_timestamp


        ######################
        # pre-event interval #
        ######################

            # Set first (i.e. latest) ongoing interval as pre-event interval.
            if(pre_event_interval_type == interval_type_end_of_day) {
              pre_event_interval_begin_timestamp = timestamp
              pre_event_interval_type = interval_type
              pre_event_interval_is_active = interval_is_active
            }
          }


        ###################
        # interval length #
        ###################

          # Calulate interval length in seconds.
          interval_seconds = end_timestamp - timestamp


        ######################
        # adjust stack depth #
        ######################

          # Move down ongoing interval stack before processing next entry.
          --depth
        }


        ###################
        # active interval #
        ###################

        interval_is_active{

          # Add active top level interval length to total activity counter.
          # Nested active intervals are skipped to avoid double-counting.
          if(interval_type_is_top_level) {
            active_seconds += interval_seconds
          }

          # All necessary information for active interval begins has been
          # processed at this point.
          next
        }


        #####################
        # inactive interval #
        #####################

        {
            # Subtract inactive interval length from total activity counter.
            active_seconds -= interval_seconds
        }


        ##############
        # loose ends #
        ##############

        # Abort with error if ongoing interval stack is not empty in the end,
        # i.e. interval ends have been added whithout encountering a
        # corresponding start later on.
        # Note: Only the first (i.e. latest) loose end is included in the error
        # message, but all are cleaned up before aborting.
        END{
          if(length(depth2interval_type)) {
            for(depth in depth2interval_type) {
              if(!loose_end) {
                loose_end = depth2end_entry[depth]
              }
              delete depth2end_entry[depth]
              delete depth2interval_type[depth]
              delete depth2end_timestamp[depth]
            }
            print_error(loose_end_message loose_end "'"'"'")
          }


        #############################
        # pre-event interval length #
        #############################

        # Calulate pre-event interval length in seconds.
        # Note: This needs to be calculated in either the `BEGIN` or `END` rule
        # to ensure it exists for empty input. Calculating it here saves the
        # need to track it compared to initializing it in the `BEGIN` rule.
        pre_event_interval_seconds = \
          event_timestamp - pre_event_interval_begin_timestamp


        ###########################
        # pre-event (in-)activity #
        ###########################

          # Report total activity during active intervals, but only current
          # inactivity during inactive intervals.
            pre_event_in_active_seconds = \
              pre_event_interval_is_active \
                ? active_seconds \
                : pre_event_interval_seconds


        ##################
        # pre-event data #
        ##################

          # Print pre-event information.
          print pre_event_interval_seconds, pre_event_interval_type,
            pre_event_in_active_seconds
        }
      '
) || print_error "$INVALID_TSV_MESSAGE"

# Split pre-event data into single fields.
PRE_EVENT_INTERVAL_SECONDS=$(printf '%s' "$PRE_EVENT_DATA" \
                             | awk --field-separator='\t' '{printf $1}')
PRE_EVENT_INTERVAL_TYPE=$(printf '%s' "$PRE_EVENT_DATA" \
                          | awk --field-separator='\t' '{printf $2}')
PRE_EVENT_INTERVAL_IN_ACTIVE_SECONDS=$(printf '%s' "$PRE_EVENT_DATA" \
                                       | awk --field-separator='\t' \
                                           '{printf $3}')


#####################
# commute direction #
#####################

if [ "$COMMAND" = "$COMMUTE_COMMAND" ]
then
  case "$PRE_EVENT_INTERVAL_TYPE" in
    "$INTERVAL_TYPE_END_OF_DAY"|"$INTERVAL_TYPE_HOMEOFFICE")
      COMMAND="$COMMUTE_TO_WORK_COMMAND"
      ;;
    "$INTERVAL_TYPE_WORK")
      COMMAND="$COMMUTE_HOME_COMMAND"
      ;;
    *)
      print_error "$UNCLEAR_COMMUTE_DIRECTION_MESSAGE"
      ;;
  esac
fi


#############################
# pre-commute interval type #
#############################


# Get pre-commute interval type from time tracking TSV file.
if [ "$PRE_EVENT_INTERVAL_TYPE" = "$INTERVAL_TYPE_COMMUTE" ]
then
  # Extract pre-commute interval end entry from time tracking TSV file.
  PRE_COMMUTE_INTERVAL_END_TSV_ENTRY=$(
    tac "$TIME_TRACKING_TSV" \
    | awk \
        --field-separator='\t' \
        '
          $'"$TSV_COLUMN_INDICATOR"' == "'"$BEGIN_INDICATOR"'" &&
            $'"$TSV_COLUMN_INTERVAL_TYPE"' == "'"$INTERVAL_TYPE_COMMUTE"'" {
            getline
            print
            exit
          }
        '
  )

  # Extract interval type from pre-commute interval end TSV entry.
  PRE_COMMUTE_INTERVAL_TYPE=$(
     printf '%s' "$PRE_COMMUTE_INTERVAL_END_TSV_ENTRY" \
     | awk --field-separator='\t' '{printf $'"$TSV_COLUMN_INTERVAL_TYPE"'}'
  )

  # Compare adjenct commute interval times to identify turn-around
  # commutes.
  if [ "$PRE_COMMUTE_INTERVAL_TYPE" = "$INTERVAL_TYPE_COMMUTE" ]
  then

    # Extract previous commute end timestamp from pre-commute interval
    # end TSV entry.
    PRE_COMMUTE_INTERVAL_END_TIMESTAMP=$(
       printf '%s' "$PRE_COMMUTE_INTERVAL_END_TSV_ENTRY" \
       | awk --field-separator='\t' '{printf $'"$TSV_COLUMN_TIMESTAMP"'}'
    )

    # Extract current commute begin timestamp from lates (i.e. most
    # recent) TSV entry.
    COMMUTE_INTERVAL_BEGIN_TIMESTAMP=$(
      tac "$TIME_TRACKING_TSV" \
      | awk --field-separator='\t' '{printf $'"$TSV_COLUMN_TIMESTAMP"'; exit}'
    )

    # Set pre-commute interval type to (zero time) work interval for
    # turn-around commutes.
    if [ "$PRE_COMMUTE_INTERVAL_END_TIMESTAMP" = \
           "$COMMUTE_INTERVAL_BEGIN_TIMESTAMP" ]
    then
      PRE_COMMUTE_INTERVAL_TYPE="$INTERVAL_TYPE_WORK"
    fi
  fi
fi


#########################
# status message prefix #
#########################

# Set status message prefix based on pre-event interval type.
case "$PRE_EVENT_INTERVAL_TYPE" in
  "$INTERVAL_TYPE_HOMEOFFICE")
    STATUS_MESSAGE_PREFIX="$STATUS_MESSAGE_PREFIX_HOMEOFFICE"
    ;;
  "$INTERVAL_TYPE_BREAK")
    STATUS_MESSAGE_PREFIX="$STATUS_MESSAGE_PREFIX_BREAK"
    ;;
  "$INTERVAL_TYPE_CALL")
    STATUS_MESSAGE_PREFIX="$STATUS_MESSAGE_PREFIX_CALL"
    ;;
  "$INTERVAL_TYPE_COMMUTE")
    case "$PRE_COMMUTE_INTERVAL_TYPE" in
      "$INTERVAL_TYPE_COMMUTE"|"$INTERVAL_TYPE_HOMEOFFICE")
        STATUS_MESSAGE_PREFIX="$STATUS_MESSAGE_PREFIX_COMMUTE_TO_WORK"
        ;;
      "$INTERVAL_TYPE_WORK")
        STATUS_MESSAGE_PREFIX="$STATUS_MESSAGE_PREFIX_COMMUTE_HOME"
        ;;
      *)
        print_error "$UNREACHABLE_STATEMENT_MESSAGE"
        ;;
    esac
    ;;
  "$INTERVAL_TYPE_WORK")
    STATUS_MESSAGE_PREFIX="$STATUS_MESSAGE_PREFIX_WORK"
    ;;
  "$INTERVAL_TYPE_MOBILE_WORK")
    STATUS_MESSAGE_PREFIX="$STATUS_MESSAGE_PREFIX_MOBILE_WORK"
    ;;
  "$INTERVAL_TYPE_END_OF_DAY")
    STATUS_MESSAGE_PREFIX="$STATUS_MESSAGE_PREFIX_END_OF_DAY"
    ;;
  *)
    print_error "$UNREACHABLE_STATEMENT_MESSAGE"
    ;;
esac


########################
# status interval time #
########################

# Pass pre-event interval time through `date` for formatting to get status
# inactivity time prefix.
# Skip reporting if covered by (in-)activity, add separator otherwise.
if [ "$PRE_EVENT_INTERVAL_SECONDS" -lt \
     "$PRE_EVENT_INTERVAL_IN_ACTIVE_SECONDS" ]
then
  STATUS_TIME_PREFIX=$(
    date \
      --universal \
      --date='@'"$PRE_EVENT_INTERVAL_SECONDS" \
      '+'"$STATUS_TIME_FMT$STATUS_TIME_SEPARATOR"
  )
fi


###########################
# status (in-)active time #
###########################

# Pass pre-event (in-)active time through `date` for formatting to get status
# time.
STATUS_INACTIVE_TIME=$(
  date \
    --universal \
    --date='@'"$PRE_EVENT_INTERVAL_IN_ACTIVE_SECONDS" '+'"$STATUS_TIME_FMT"
)

#########################
# status message suffix #
#########################

# Set status message suffix base on time strings.
STATUS_SUFFIX="[$STATUS_TIME_PREFIX$STATUS_INACTIVE_TIME]"


##################
# status message #
##################

# Concatenate status message.
STATUS_MESSAGE="$STATUS_MESSAGE_PREFIX $STATUS_SUFFIX"

# Print status message and exit if `status` command was given.
if [ "$COMMAND" = "$STATUS_COMMAND" ]
then
  printf '%s\n' "$STATUS_MESSAGE"
  exit
fi


##########################################
# pre-commute work / commute turn-around #
##########################################

if [ "$COMMAND" = "$COMMUTE_TO_WORK_COMMAND" ]
then
  case "$PRE_EVENT_INTERVAL_TYPE" in
    "$INTERVAL_TYPE_END_OF_DAY")
      ;;
    "$INTERVAL_TYPE_HOMEOFFICE")
      printf '%s\t%s\t%s\n' "$EVENT_TIMESTAMP" "$END_INDICATOR" \
        "$PRE_EVENT_INTERVAL_TYPE" >> "$TIME_TRACKING_TSV"
      ;;
    *)
      print_error "$INVALID_PRE_COMMUTE_INTERVAL_MESSAGE"\
"$PRE_EVENT_INTERVAL_TYPE'"
      ;;
  esac
fi
if [ "$COMMAND" = "$COMMUTE_HOME_COMMAND" ]
then
  case "$PRE_EVENT_INTERVAL_TYPE" in
    "$INTERVAL_TYPE_COMMUTE"|"$INTERVAL_TYPE_WORK")
      printf '%s\t%s\t%s\n' "$EVENT_TIMESTAMP" "$END_INDICATOR" \
        "$PRE_EVENT_INTERVAL_TYPE" >> "$TIME_TRACKING_TSV"
      ;;
    *)
      print_error "$INVALID_PRE_COMMUTE_INTERVAL_MESSAGE"\
"$PRE_EVENT_INTERVAL_TYPE'"
      ;;
  esac
fi


##############################
# pre-arrival interval check #
##############################

if [ "$COMMAND" = "$ARRIVE_COMMAND" ]
then
  [ "$PRE_EVENT_INTERVAL_TYPE" = "$INTERVAL_TYPE_COMMUTE" ] ||
  print_error "$INVALID_PRE_ARRIVAL_INTERVAL_MESSAGE"\
"$PRE_EVENT_INTERVAL_TYPE'"
fi


#############
# indicator #
#############

# Set indicator based on command.
case "$COMMAND" in
  "$START_COMMAND"|"$PAUSE_COMMAND"|"$CALL_COMMAND"|"$COMMUTE_TO_WORK_COMMAND"|"$COMMUTE_HOME_COMMAND")
    INDICATOR="$BEGIN_INDICATOR"
    ;;
  "$STOP_COMMAND"|"$CONTINUE_COMMAND"|"$HANGUP_COMMAND"|"$ARRIVE_COMMAND")
    INDICATOR="$END_INDICATOR"
    ;;
  *)
    print_error "$UNREACHABLE_STATEMENT_MESSAGE"
    ;;
esac


#################
# interval type #
#################

# Set interval type based on command.
case "$COMMAND" in
  "$START_COMMAND")
    case "$PRE_EVENT_INTERVAL_TYPE" in
      "$INTERVAL_TYPE_END_OF_DAY")
        INTERVAL_TYPE="$INTERVAL_TYPE_HOMEOFFICE"
        ;;
      "$INTERVAL_TYPE_COMMUTE")
        INTERVAL_TYPE="$INTERVAL_TYPE_MOBILE_WORK"
        ;;
    *)
      print_error "$INVALID_PRE_HOMEOFFICE_INTERVAL_MESSAGE"\
"$PRE_EVENT_INTERVAL_TYPE'"
      ;;
    esac
  ;;
  "$STOP_COMMAND")
    case "$PRE_EVENT_INTERVAL_TYPE" in
      "$INTERVAL_TYPE_HOMEOFFICE"|"$INTERVAL_TYPE_MOBILE_WORK")
        INTERVAL_TYPE="$PRE_EVENT_INTERVAL_TYPE"
        ;;
    *)
      print_error "$UNCLEAR_INTERVAL_TO_STOP_MESSAGE"\
"$PRE_EVENT_INTERVAL_TYPE'"
      ;;
    esac
    ;;
  "$PAUSE_COMMAND"|"$CONTINUE_COMMAND")
    INTERVAL_TYPE="$INTERVAL_TYPE_BREAK"
    ;;
  "$CALL_COMMAND"|"$HANGUP_COMMAND")
    INTERVAL_TYPE="$INTERVAL_TYPE_CALL"
    ;;
  "$COMMUTE_TO_WORK_COMMAND"|"$COMMUTE_HOME_COMMAND"|"$ARRIVE_COMMAND")
    INTERVAL_TYPE="$INTERVAL_TYPE_COMMUTE"
    ;;
  *)
    print_error "$UNREACHABLE_STATEMENT_MESSAGE"
    ;;
esac


###########################
# write time tracking TSV #
###########################

# Write time tracking event data to TSV file.
printf '%s\t%s\t%s\n' "$EVENT_TIMESTAMP" "$INDICATOR" "$INTERVAL_TYPE" \
  >> "$TIME_TRACKING_TSV"


#############################
# pre-commute interval type #
#############################

# Get pre-commute interval type from time tracking TSV file.
if [ "$COMMAND" = "$COMMUTE_TO_WORK_COMMAND" ] ||
   [ "$COMMAND" = "$COMMUTE_HOME_COMMAND" ]
then

  # Set pre-commute interval to (zero time) work for turn-around
  # commutes.
  if [ -n "$PRE_COMMUTE_INTERVAL_TYPE" ]
  then
    [ "$COMMAND" = "$COMMUTE_HOME_COMMAND" ] ||
      print_error "$UNREACHABLE_STATEMENT_MESSAGE"
    [ "$PRE_COMMUTE_INTERVAL_TYPE" = "$INTERVAL_TYPE_COMMUTE" ] ||
      print_error "$UNREACHABLE_STATEMENT_MESSAGE"
    PRE_COMMUTE_INTERVAL_TYPE="$INTERVAL_TYPE_WORK"

  # Extract pre-commute interval end entry from time tracking TSV file
  # for regular commutes.
  else
    PRE_COMMUTE_INTERVAL_END_TSV_ENTRY=$(
      tac "$TIME_TRACKING_TSV" \
      | awk \
          --field-separator='\t' \
          '
            $'"$TSV_COLUMN_INDICATOR"' == "'"$BEGIN_INDICATOR"'" &&
              $'"$TSV_COLUMN_INTERVAL_TYPE"' == "'"$INTERVAL_TYPE_COMMUTE"'" {
              getline
              print
              exit
            }
          '
    )

    # Extract interval type from pre-commute interval end TSV entry.
    PRE_COMMUTE_INTERVAL_TYPE=$(
       printf '%s' "$PRE_COMMUTE_INTERVAL_END_TSV_ENTRY" \
       | awk --field-separator='\t' '{printf $'"$TSV_COLUMN_INTERVAL_TYPE"'}'
    )
  fi
fi


#########################
# post-commute interval #
#########################

# Set post-commute interval type based on pre-commute one and start work
# interval if arriving at work.
if [ "$COMMAND" = "$ARRIVE_COMMAND" ]
then
  case "$PRE_COMMUTE_INTERVAL_TYPE" in
    "$INTERVAL_TYPE_HOMEOFFICE"|"$INTERVAL_TYPE_COMMUTE")
      POST_COMMUTE_INTERVAL_TYPE="$INTERVAL_TYPE_WORK"
      printf '%s\t%s\t%s\n' "$EVENT_TIMESTAMP" "$BEGIN_INDICATOR" \
        "$POST_COMMUTE_INTERVAL_TYPE" >> "$TIME_TRACKING_TSV"
      ;;
    "$INTERVAL_TYPE_WORK")
      POST_COMMUTE_INTERVAL_TYPE="$INTERVAL_TYPE_END_OF_DAY"
      ;;
    *)
      print_error "$UNREACHABLE_STATEMENT_MESSAGE"
      ;;
  esac
fi


################
# Slack status #
################

# Only continue if an OAuth token to update Slack status with was given.
[ -n "$SLACK_TOKEN" ] || exit


############################
# post-event interval type #
############################

# Initialize post-event interval type with current interval type.
POST_EVENT_INTERVAL_TYPE="$INTERVAL_TYPE"

# Overwrite post-event interval type based on command for interval ends.
if [ "$INDICATOR" = "$END_INDICATOR" ]
then
  case "$COMMAND" in

    # Set interval type to after-hours or mobile work (based on
    # interval type) after `stop` command.
    "$STOP_COMMAND")
      case "$INTERVAL_TYPE" in
        "$INTERVAL_TYPE_HOMEOFFICE")
          POST_EVENT_INTERVAL_TYPE="$INTERVAL_TYPE_END_OF_DAY"
          ;;
        "$INTERVAL_TYPE_MOBILE_WORK")
          INTERVAL_TYPE="$INTERVAL_TYPE_COMMUTE"
          ;;
      *)
        print_error "$UNREACHABLE_STATEMENT_MESSAGE"
        ;;
      esac
      ;;

    # Set interval type to post-commute one after `arrive` command.
    "$ARRIVE_COMMAND")
      POST_EVENT_INTERVAL_TYPE="$POST_COMMUTE_INTERVAL_TYPE"
      ;;

    # Extract previous non-break/call/commute interval begin from time
    # tracking TSV file and set interval status to its interval type for
    # `continue`/`hangup` commands.
    # TODO: Add sanity checks and error handling.
    "$CONTINUE_COMMAND"|"$HANGUP_COMMAND")

      # Extract previous non-break/call interval begin from time
      # tracking TSV file.
      PREVIOUS_INTERVAL_BEGIN_TSV_ENTRY=$(
        tac "$TIME_TRACKING_TSV" \
        | awk \
            --field-separator='\t' \
            '
              $'"$TSV_COLUMN_INDICATOR"' == "'"$BEGIN_INDICATOR"'" &&
                $'"$TSV_COLUMN_INTERVAL_TYPE"' != "'"$INTERVAL_TYPE_BREAK"'" &&
                $'"$TSV_COLUMN_INTERVAL_TYPE"' != "'"$INTERVAL_TYPE_CALL"'" {
                print
                exit
              }
            '
      )

      # Extract interval type from previous non-break/call/commute
      # interval begin TSV entry.
      PREVIOUS_INTERVAL_TYPE=$(
         printf '%s' "$PREVIOUS_INTERVAL_BEGIN_TSV_ENTRY" \
         | awk --field-separator='\t' '{printf $'"$TSV_COLUMN_INTERVAL_TYPE"'}'
      )

      # Overwrite post-event interval type with previous
      # non-break/call/commute interval type.
      POST_EVENT_INTERVAL_TYPE="$PREVIOUS_INTERVAL_TYPE"
      ;;

    *)
      print_error "$UNREACHABLE_STATEMENT_MESSAGE"
      ;;
  esac
fi


########################
# post-event timestamp #
########################

# Initialize post-event time stamp with event time stamp.
POST_EVENT_TIMESTAMP="$EVENT_TIMESTAMP"

# Overwrite post-event time stamp for `continue`/`hangup`/`arrive`
# commands with latest non-break/call/commute interval begin time stamp.
if [ "$COMMAND" = "$CONTINUE_COMMAND" ] ||
   [ "$COMMAND" = "$HANGUP_COMMAND" ] ||
   [ "$COMMAND" = "$ARRIVE_COMMAND" ]
then
  PREVIOUS_INTERVAL_BEGIN_TIMESTAMP=$(
     printf '%s' "$PREVIOUS_INTERVAL_BEGIN_TSV_ENTRY" \
     | awk --field-separator='\t' '{printf $'"$TSV_COLUMN_TIMESTAMP"'}'
  )
  POST_EVENT_TIMESTAMP="$PREVIOUS_INTERVAL_BEGIN_TIMESTAMP"
fi


##############################
# post-event activity status #
##############################

# Set post-event activity status based on post-event interval type.
case "$POST_EVENT_INTERVAL_TYPE" in
  "$INTERVAL_TYPE_HOMEOFFICE")
    POST_EVENT_ACTIVITY_STATUS="$ACTIVITY_STATUS_HOMEOFFICE"
    ;;
  "$INTERVAL_TYPE_BREAK")
    POST_EVENT_ACTIVITY_STATUS="$ACTIVITY_STATUS_BREAK"
    ;;
  "$INTERVAL_TYPE_CALL")
    POST_EVENT_ACTIVITY_STATUS="$ACTIVITY_STATUS_CALL"
    ;;
  "$INTERVAL_TYPE_COMMUTE")
    POST_EVENT_ACTIVITY_STATUS="$ACTIVITY_STATUS_COMMUTE"
    ;;
  "$INTERVAL_TYPE_WORK")
    POST_EVENT_ACTIVITY_STATUS="$ACTIVITY_STATUS_WORK"
    ;;
  "$INTERVAL_TYPE_MOBILE_WORK")
    POST_EVENT_ACTIVITY_STATUS="$ACTIVITY_STATUS_MOBILE_WORK"
    ;;
  "$INTERVAL_TYPE_END_OF_DAY")
    POST_EVENT_ACTIVITY_STATUS="$ACTIVITY_STATUS_END_OF_DAY"
    ;;
  *)
    print_error "$UNREACHABLE_STATEMENT_MESSAGE"
    ;;
esac


######################
# status text prefix #
######################

# Set status text prefix based on post-event interval type.
# Distinguish commute direction by pre-commute interval type.
case "$POST_EVENT_INTERVAL_TYPE" in
  "$INTERVAL_TYPE_HOMEOFFICE")
    STATUS_TEXT_PREFIX="$STATUS_TEXT_PREFIX_HOMEOFFICE"
    ;;
  "$INTERVAL_TYPE_BREAK")
    STATUS_TEXT_PREFIX="$STATUS_TEXT_PREFIX_BREAK"
    ;;
  "$INTERVAL_TYPE_CALL")
    STATUS_TEXT_PREFIX="$STATUS_TEXT_PREFIX_CALL"
    ;;
  "$INTERVAL_TYPE_COMMUTE")
    case "$PRE_COMMUTE_INTERVAL_TYPE" in
      "$INTERVAL_TYPE_HOMEOFFICE"|"$INTERVAL_TYPE_COMMUTE")
        STATUS_TEXT_PREFIX="$STATUS_TEXT_PREFIX_COMMUTE_TO_WORK"
        ;;
      "$INTERVAL_TYPE_WORK")
        STATUS_TEXT_PREFIX="$STATUS_TEXT_PREFIX_COMMUTE_HOME"
        ;;
      *)
        print_error "$UNREACHABLE_STATEMENT_MESSAGE"
        ;;
    esac
    ;;
  "$INTERVAL_TYPE_WORK")
    STATUS_TEXT_PREFIX="$STATUS_TEXT_PREFIX_WORK"
    ;;
  "$INTERVAL_TYPE_MOBILE_WORK")
    STATUS_TEXT_PREFIX="$STATUS_TEXT_PREFIX_MOBILE_WORK"
    ;;
  "$INTERVAL_TYPE_END_OF_DAY")
    STATUS_TEXT_PREFIX="$STATUS_TEXT_PREFIX_END_OF_DAY"
    ;;
  *)
    print_error "$UNREACHABLE_STATEMENT_MESSAGE"
    ;;
esac


#########################
# status text timestamp #
#########################

# Set status text time stamp to post-event time stamp.
STATUS_TEXT_TIMESTAMP="$POST_EVENT_TIMESTAMP"


######################
# status text suffix #
######################

# Pass status text time stamp through `date` for formatting to get status text suffix.
STATUS_TEXT_SUFFIX=$(
  date --date="$STATUS_TEXT_TIMESTAMP" '+'"$STATUS_TEXT_TIMESTAMP_FMT"
)


###############
# status text #
###############

# Concatenate status text.
STATUS_TEXT="$STATUS_TEXT_PREFIX $STATUS_TEXT_SUFFIX"


################
# status emoji #
################

# Set status emoji based on post-event interval type.
case "$POST_EVENT_INTERVAL_TYPE" in
  "$INTERVAL_TYPE_HOMEOFFICE")
    STATUS_EMOJI="$STATUS_EMOJI_HOMEOFFICE"
    ;;
  "$INTERVAL_TYPE_BREAK")
    STATUS_EMOJI="$STATUS_EMOJI_BREAK"
    ;;
  "$INTERVAL_TYPE_CALL")
    STATUS_EMOJI="$STATUS_EMOJI_CALL"
    ;;
  "$INTERVAL_TYPE_COMMUTE")
    STATUS_EMOJI="$STATUS_EMOJI_COMMUTE"
    ;;
  "$INTERVAL_TYPE_WORK")
    STATUS_EMOJI="$STATUS_EMOJI_WORK"
    ;;
  "$INTERVAL_TYPE_MOBILE_WORK")
    STATUS_EMOJI="$STATUS_EMOJI_MOBILE_WORK"
    ;;
  "$INTERVAL_TYPE_END_OF_DAY")
    STATUS_EMOJI="$STATUS_EMOJI_END_OF_DAY"
    ;;
  *)
    print_error "$UNREACHABLE_STATEMENT_MESSAGE"
    ;;
esac


###################
# request command #
###################

# HTTP request command.
REQUEST_COMMAND="$REQUEST_METHOD"


#########################
# authentication header #
#########################

# HTTP header used for authorization.
AUTH_HEADER="$AUTH_HEADER_NAME"': '"$AUTH_METHOD $SLACK_TOKEN"


################
# content type #
################

# (Internet) media (cf. MIME) type used for request content.
CONTENT_TYPE="$CONTENT_TYPE_TYPE"'/'"$CONTENT_TYPE_SUBTYPE"

# Character set specification used for request content.
CHARSET_SPECIFICATION="$CHARSET_PARAM_NAME"'='"$CHARSET"

# (Internet) media (cf. MIME) type used for request content incl. character set
# specification.
CONTENT_TYPE_WITH_CHARSET="$CONTENT_TYPE"'; '"$CHARSET_SPECIFICATION"

# HTTP header used to specify the request content type.
CONTENT_HEADER="$CONTENT_HEADER_NAME"': '"$CONTENT_TYPE_WITH_CHARSET"


##########################
# status request content #
##########################

# JSON code for Slack user profile status settings.
USER_PROFILE_JSON='{
  "'"$STATUS_TEXT_OBJECT_NAME"'": "'"$STATUS_TEXT"'",
  "'"$STATUS_EMOJI_OBJECT_NAME"'": "'"$STATUS_EMOJI"'",
  "'"$STATUS_EXPIRATION_OBJECT_NAME"'": '"$STATUS_EXPIRATION"'
}'

# JSON code for status request content.
STATUS_REQUEST_CONTENT_JSON='{
  "'"$USER_PROFILE_OBJECT_NAME"'": '"$USER_PROFILE_JSON"'
}'


##################
# status request #
##################

# Send status HTTP request to host server.
curl \
  --no-progress-meter \
  --output /dev/stderr \
  --request "$REQUEST_COMMAND" \
  --header "$AUTH_HEADER" \
  --header "$CONTENT_HEADER" \
  --data "$STATUS_REQUEST_CONTENT_JSON" \
  "$HOST_URI_PREFIX$HOST_URI_SUFFIX_STATUS"


############
# presence #
############

# Set presence setting based on activity status.
case "$POST_EVENT_ACTIVITY_STATUS" in
  "$ACTIVITY_STATUS_ACTIVE")
    PRESENCE_SETTING="$PRESENCE_SETTING_ACTIVE"
    ;;
  "$ACTIVITY_STATUS_INACTIVE")
    PRESENCE_SETTING="$PRESENCE_SETTING_INACTIVE"
    ;;
  *)
    print_error "$UNREACHABLE_STATEMENT_MESSAGE"
    ;;
esac


############################
# presence request content #
############################

# JSON code for presence request content.
PRESENCE_REQUEST_CONTENT_JSON='{
  "'"$USER_PRESENCE_OBJECT_NAME"'": "'"$PRESENCE_SETTING"'"
}'


####################
# presence request #
####################

# Send presence HTTP request to host server.
curl \
  --no-progress-meter \
  --output /dev/stderr \
  --request "$REQUEST_COMMAND" \
  --header "$AUTH_HEADER" \
  --header "$CONTENT_HEADER" \
  --data "$PRESENCE_REQUEST_CONTENT_JSON" \
  "$HOST_URI_PREFIX$HOST_URI_SUFFIX_PRESENCE"
