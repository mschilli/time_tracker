# time_tracker

Working hours time tracking tool.


## License

Copyright (C) 2020, 2021
Marcel Schilling

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details.

You should have received [a copy of the GNU Affero General Public
License](LICENSE) along with this program.
If not, see <http://www.gnu.org/licenses/>.


## Dependencies

 * POSIX shell
 * `tac` (provided by [GNU coreutils][coreutils])
 * `wc` (provided by [GNU coreutils][coreutils])
 * `date` (provided by [GNU coreutils][coreutils])
 * [sed](https://www.gnu.org/software/sed/)
 * [(GNU) awk](https://www.gnu.org/software/gawk/)
 * [cURL](https://curl.haxx.se) (optional, required for Slack status updates)

[coreutils]: https://www.gnu.org/software/coreutils/coreutils.html

## Usage

```sh
time_tracker <command> <TSV-file> [Slack-token]
```

The following commands are currently supported:

 * `start` - Start working in remotely or mobile.
 * `stop` - Stop working in remotely or mobile.
 * `pause` - Start a break.
 * `continue` - Continue working after a break.
 * `status` - Query current status.
 * `call` - Start a (phone/video) call.
 * `hangup` - Continue working after a call.
 * `commute_to_work` - Start commuting to work (inactive).
 * `commute_home` - Start commuting from work (inactive).
 * `commute` - Start commuting from/to work (inactive).
 * `arrive` - Start working or end the work day after a commute.

The following parameter is required:

 * `TSV-file` - File to read/write time tracking data from/to.

The following parameter is optional:

 * `Slack-token` - OAuth token to update Slack status with.
                   Requires `users.profile:write` and `users.write` user token
                   scopes.

(On side) work hours are determined by their adjacent commute (to and from)
work intervals and thus do not require manual handling.

Note: Only some sanity checks are made at this point. Issuing commands in the
wrong order could invalidate the TSV file.
